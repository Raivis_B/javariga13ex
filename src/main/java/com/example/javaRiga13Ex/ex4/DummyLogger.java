package com.example.javaRiga13Ex.ex4;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;


@Slf4j
public class DummyLogger {

    private void sayHi() {
        log.info("Hello from ex4!");
    }
}
