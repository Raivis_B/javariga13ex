package com.example.javaRiga13Ex.ex3;


import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

@Service


public class SecondaryLoggerHolder implements CommandLineRunner {

    private final DummyLogger dummyLogger;

    public SecondaryLoggerHolder( @Qualifier("dummyLoggerSecondary")DummyLogger dummyLogger) {
        this.dummyLogger = dummyLogger;
    }

    @Override
    public void run(String... args) throws Exception {
        dummyLogger.sayHello();
    }
}
